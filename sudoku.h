#ifndef SUDOKU_H
#define SUDOKU_H


class Sudoku
{
private:
  const short m_MAX_CNT_NUMBER = 81;
  const short m_MAP_SIZE = 9;

  short** m_Map;
  short m_ActuCntNumber;

  void Check_Field();
  bool Is_Col_Valid(short& Col, short& SearchNum);
  bool Is_Row_Valid(short& Row, short& SearchNum);
  bool Is_Sqr_Valid(short& SqrCol, short& SqrRow, short& SearchNum);
  bool Valid_Map();
public:
  Sudoku();
  ~Sudoku();
  bool Read_Map();
  void Print_Map();
};

#endif // SUDOKU_H
