#include <iostream>

#include "sudoku.h"


Sudoku::Sudoku()
  : m_ActuCntNumber(0)
{
  m_Map = new short* [m_MAP_SIZE];
  for(short i = 0; i < m_MAP_SIZE; ++i)
    m_Map[i] = new short [m_MAP_SIZE];
}

Sudoku::~Sudoku()
{
  for(short i = 0; i < m_MAP_SIZE; ++i)
    delete [] m_Map[i];
  delete [] m_Map;
}

bool Sudoku::Read_Map()
{
  char c;

  for(short row = 0; row <= m_MAP_SIZE; row++)
    for(short coll = 0; coll <= m_MAP_SIZE; coll++) {
      std::cin.get(c);

      if(coll == m_MAP_SIZE && c != '\n') {
        std::cout << "Chybne zadana mapa sudoku." << std::endl;
        std::cout << "C je: " << (ushort)c << " " << row << " " << coll << std::endl;
        return false;
      }

      if(c == '\n') continue;

      if(c >= 48 && c <= 57) {
        m_Map[row][coll] = c - '0';

        if(c == 48)
          m_ActuCntNumber++;
      }
      else {
        std::cout << "Precten neplatny znak \"" << c << "\"." << std::endl;
        return false;
      }
    }

  return true;
}

void Sudoku::Print_Map()
{
  for(short row = 0; row < m_MAP_SIZE; row++) {
    for(short coll = 0; coll < m_MAP_SIZE; coll++)
      std::cout << m_Map[row][coll];
    std::cout << std::endl;
  }
}

bool Sudoku::Is_Col_Valid(short& Col, short& SearchNum)
{
  for(short i = 0; i < m_MAP_SIZE; i++)
    if(m_Map[i][Col] == SearchNum) return false;

  return true;
}

bool Sudoku::Is_Row_Valid(short& Row)
{
  for(short i = 0; i < m_MAP_SIZE; i++)
    if(m_Map[Row][i] == SearchNum) return false;

  return true;
}

bool Sudoku::Is_Sqr_Valid(short& SqrCol, short& SqrRow, short& SearchNum)
{ // prochazeni ctverce 3x3
  for(short SqrRow = 0; SqrRow < SqrRow + 3; SqrRow++)
    for(short SqrCol = 0; SqrCol < SqrCol + 3; SqrCol++)
      if(m_Map[SqrRow][SqrCol] == SearchNum) return false;

  return true;
}

